import Sketch from "sketch-js";
import {  green, yellow, cyan, pink } from "@material-ui/core/colors";

export default function initSk(container) {

    var COLOURS = [ yellow[400], green[400], pink[300],cyan[300],'#fff']
    var radius = 0;
    var randomColor = 0;
    let abs = Math.abs
    let sin = Math.sin
    let count = 0;
    Sketch.create({
        container,
        autoclear: false,
        retina: 'auto',
        interval: 6,

        update: function() {
            radius = 2 + abs( sin( this.millis * 0.003 ) * 50 );
            count++;
            if (count === 25) {
                count = 0;
                randomColor = parseInt( 0 + (COLOURS.length - 0) * Math.random(), 10 )
            }
        },

        touchmove: function() {

            for ( var i = this.touches.length - 1, touch; i >= 0; i-- ) {

                touch = this.touches[i];

                this.lineCap = 'round';
                this.lineJoin = 'round';
                this.fillStyle = this.strokeStyle = COLOURS[ randomColor % COLOURS.length ];
                this.lineWidth = radius;

                this.beginPath();
                this.moveTo( touch.ox, touch.oy );
                this.lineTo( touch.x, touch.y );
                this.stroke();
            }
        }
    });
}
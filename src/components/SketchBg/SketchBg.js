import React from "react";

export default function SketchBg() {
  return (
    <div className="background-wrapper">
      <div id="background-content"></div>
    </div>
  );
}

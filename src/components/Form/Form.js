import React from "react";

import { FormControl, FormGroup, TextField } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import IconButton from "@material-ui/core/IconButton";

function Form() {
  return (
    <Grid item xs={12}>
      <form className="form">
        <FormControl style={{ width: "100%" }}>
          <FormGroup style={{ position: "relative" }}>
            <TextField
              className="input-test fade-in"
              type="text"
              name="email"
              placeholder="Your email"
            />
            <IconButton
              className="btn-send"
              color="primary"
              aria-label="send"
              type="submit"
            >
              <ArrowForwardIcon />
            </IconButton>
          </FormGroup>
          <FormGroup>
            <p className="note fade-in">*Only relevant information, no spam</p>
          </FormGroup>
        </FormControl>
      </form>
    </Grid>
  );
}

export default Form;

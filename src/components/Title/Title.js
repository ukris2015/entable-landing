import React from "react";

import Grid from "@material-ui/core/Grid";

function Title(props) {
  const { title, tag } = props;
  return (
    <Grid item xs={12}>
      {tag ? (
        <h1 className="font-size-90 fade-in">{title}</h1>
      ) : (
        <h2 className="opacity-60 fade-in">{title}</h2>
      )}
    </Grid>
  );
}

export default Title;

import React, { useEffect, createRef } from "react";
import sk from '../../utils/sketch'
import {  Input } from "@material-ui/core";
import './styles.css'

function LandingPage() {
    const ref = createRef()

    useEffect(() => {
      //Sk.SketchInnit()
      (function() {
        sk(ref.current)
      })()
    }, [ref])

    return (
        <div className="container" ref={ref}>
            <div className="container__center">
                <h2>Get Ready!</h2>
                <Input placeholder="Your Email"  className="container__center__input" disableUnderline={true} />
            </div>
        </div>
    );
}

export default LandingPage;

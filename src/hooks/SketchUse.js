import { useEffect, useState } from "react";

/**Library Sketch.js */
import Sketch from "sketch-js";

export default function SketchUse() {
  const [colors, setColors] = useState([
    "#61dfff",
    "#A7EBCA",
    "#FFFFFF",
    "#ff6767",
    "#61dfff",
  ]);

  useEffect(() => {
    let radius = 0;
    let randomColor = 0;

    Sketch.create({
      container: document.getElementById("background-content"),
      autoclear: false,
      retina: "auto",

      update: function () {
        radius = 2 + Math.abs(Math.sin(this.millis * 0.003) * 50);
      },
      click: function () {
        randomColor = parseInt(0 + (colors.length - 0) * Math.random(), 10);
      },

      touchmove: function () {
        for (let i = this.touches.length - 1, touch; i >= 0; i--) {
          touch = this.touches[i];

          this.lineCap = "round";
          this.lineJoin = "round";
          this.fillStyle = this.strokeStyle =
            colors[randomColor % colors.length];
          this.lineWidth = radius;

          this.beginPath();
          this.moveTo(touch.ox, touch.oy);
          this.lineTo(touch.x, touch.y);
          this.stroke();
        }
      },
    });
  });
}

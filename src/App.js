import React from "react";

import Title from "./components/Title";
import Form from "./components/Form";
import SketchBg from "./components/SketchBg";

import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";

import SketchUse from "./hooks/SketchUse";

import "./App.scss";

function App() {
  const sketch = SketchUse();

  return (
    <Container className="content-wrapper">
      <div className="content-main">
        <Grid container spacing={3}>
          <Title title="Get Ready!" tag={true} />
          <Title title="Something Awesome is Coming Soon" tag={false} />
          <Form />
        </Grid>
      </div>
      <SketchBg />
    </Container>
  );
}

export default App;
